การโปรแกรมเบื้องต้น
===============================================================================

การเขียนโปรแกรม


ตัวอย่างโปรแกรมในภาษาต่างๆ
-------------------------------------------------------------------------------

### ภาษา C
````cpp
#include <stdio.h>
 
int main() {
    int a, b;
    scanf("%d%d", &a, &b);
    int result = a + b;
    printf("%d\n", result);
    return 0;
}
````

### ภาษา C++
````cpp
#include <iostream>
 
int main() {
    int a, b;
    std::cin >> a >> b;
    int result = a + b;
    std::cout << result << endl;
    return 0;
}
````

### ภาษา Java
````java
import java.util.Scanner;
 
public class Test {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a, b;
        a = scanner.nextInt();
        b = scanner.nextInt();
        int result = a + b;
        System.out.println(result);
    }
}
````

### ภาษา Python
````python
#!/usr/bin/env python3
 
a = int(raw_input())
b = int(raw_input())
 
result = a + b
 
print (result)
````

### ภาษา C&#35;
````cs
using System;
 
public class Program {
    public static int Main() {
        int a, b;
        string line = Console.ReadLine();
        int.TryParse(line, out a);
        line = Console.ReadLine();
        int.TryParse(line, out b);
        int result = a + b;
        Console.WriteLine(result);
    }
}
````




