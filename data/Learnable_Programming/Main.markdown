Learnable Programming
===============================================================================

* [[Learnable Programming/Random Idea]] -- ไอเดียต่างๆ ของผมที่ยังไม่ค่อยเป็นรูปเป็นร่าง (ควรอ่านบทความข้างล่างให้หมดเสียก่อน)

บทความ (ภาษาอังกฤษ)
-------------------------------------------------------------------------------
### ต้นฉบับ
* [Learnable Programming](http://worrydream.com/LearnableProgramming/) - Bret Victor

### บทความตอบรับ
* http://nothings.org/computer/victory.html
  * http://soledadpenades.com/2012/09/28/on-learnable-programming-by-bret-victor/
* http://computinged.wordpress.com/2012/09/28/learnable-programming-thinking-about-programming-languages-and-systems-in-a-new-way/
* http://nearthespeedoflight.com/article/2013_05_07_thoughts_on_thoughts_on_bret_victor___s____learnable_programming___
  * http://scientopia.org/blogs/goodmath/2012/10/05/everyone-should-program-or-programming-is-hard-both/
  * http://www.technologyreview.com/view/429438/dear-everyone-teaching-programming-youre-doing-it-wrong/
  * http://academiccomputing.wordpress.com/2012/09/28/experts-can-program-blindfolded/
* http://blog.gethopscotch.com/post/32807560089/learnableprogramming
  * http://blog.gethopscotch.com/post/32871556430/learnable-programming-part-ii-dump-the-parts-bucket-on

### นำไปใช้
* https://github.com/kumavis/Learnable-Programming-Resources
* http://www.fullstack.io/choc/
* http://amasad.me/2014/01/10/implementing-bret-victors-learnable-programming-has-never-been-easier/
 

คำสำคัญ
-------------------------------------------------------------------------------
* Program flow
* Data visualization
* Standard library locked-in
* Abstraction
* Metaphor

