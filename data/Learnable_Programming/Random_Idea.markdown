Learnable Programming Idea
===============================================================================

* If we can abstract computer hardware enough that normal user can use it easily, why can't we abstract the ''computer programming'' itself?
  * This is actually just a prove that it is very well possible to implement learnable programming.
* Programming is synthesising. If you don't have synthesising skill, you can't into programming.
* You can only learn so much in the same time: problem with learning higher-level language like Processing or Javascript is that you '''MUST''' learn both the programming itself (program flow/logic/etc) at well as learn those API at the same time. (This is forgotten by many experienced programmer)
* Why many language aren't suitable to be beginning:
  * PHP/Python/Ruby/JS (console): Hard to read stdin.
  * Java/C#: cumbersome skeleton and verbose stdio operation
  * Processing: Too many distractions
  * C/C++: too low, but it may actually works quite well.
  * Go: like C/C++, but a little higher?
  * HTML/CSS: you are kidding, right?
  * ''...did I missed any popular beginner language?''
* Graphics processing is very good learning tools
  * See `conio.h` for DOS program. Very easy to play with.
* Very comprehensive help library required (to help kickstart self-study and encourage RTFM)
  * Turbo C++ is a very good example of this.
  * But if tools is too easy to be accessed it might caused problem.
    * Real-time syntax checking might be bad for beginner.
* Good debugging/visualization tool (original Learnable Programming idea)
  * Direct virtualisation might not be as important, but auto-stepping should do trick.

Selecting candidates
--------------------
* Potential for being programmer:
  * Ability to synthesise (the hardest abilites)
  * Test for devious way to solve some staged problem (may also be in paper)
  * Test logic (standard IQ test should suffice)

Training good programmer
------------------------
* Encourage to write test?


